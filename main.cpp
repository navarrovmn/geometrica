#include "triangulo.hpp"
#include "quadrado.hpp"
#include "retangulo.hpp"

#include <iostream>

using namespace std;

int main(){

	triangulo umTriangulo(10, 30);
	quadrado umQuadrado(50,50);
	cout << "Area: " << umTriangulo.area() << endl;
	cout << "Area: " << umQuadrado.area() << endl;

	return 0;
}

