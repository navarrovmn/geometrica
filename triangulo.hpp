#ifndef _TRIANGULO_H_
#define _TRIANGULO_H_

#include "geometrica.hpp"

class triangulo:public geometrica{
	public:
	triangulo();
	triangulo(float base, float altura);

	float area();
};

#endif
